CREATE TABLE customer (
  ID int NOT NULL AUTO_INCREMENT,
  Name varchar(100) NOT NULL,
  Surname varchar(100) NOT NULL,
  Regplate varchar(100) NOT NULL,
  Country varchar(100) NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB;


INSERT INTO customer (Name,Surname,Regplate, Country) VALUES
     ('Elitsa','Hrastova','CB 3080 HP','Bulgaria'),
     ('Deyan','Gospodinov','CA 73330 KH','Bulgaria'),
     ('Radostin','Asenov','C 0416 XH','Bulgaria'),
     ('Vancho','Stojanovski','VE 4593','North Macedonia'),
     ('Petar','Mitrevski','SR 1029 AB','North Macedonia'),
     ('Stole','Petreovski','SK 7904 AD','North Macedonia'),
     ('Mirko','Radoman','PG AC 305','Montenegro'),
     ('Stevan','Strugar','DG DD 706','Montenegro'),
     ('Adam','Potpara','KODR808','Montenegro'),
     ('Talat','Bulut','06 C 6970','Turkey');
INSERT INTO customer (Name,Surname,Regplate,Country) VALUES
     ('Mehmet','Yasar','06 C 6970','Turkey'),
     ('Aslan','Sipahi','17 D 4102','Turkey'),
     ('Antonis','Spiros','IBH-4596','Greece'),
     ('Andreas','Marinakis','NBA-2807','Greece'),
     ('Petros','Ninis','NZC-2107','Greece'),
     ('Zvezdan','Prevjak','M47-A-103','Bosnia And Herzegovina'),
     ('Asmir','Osim','H43-B-200','Bosnia And Herzegovina'),
     ('Ibrahim','Katalinski','H40-C-310','Bosnia And Herzegovina');
